package com.zzw.generatesql.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.zzw.generatesql.constant.HaveRemark;
import com.zzw.generatesql.constant.HaveSemicolon;
import com.zzw.generatesql.constant.ObligatoryMode;
import com.zzw.generatesql.entity.ColumnName;
import com.zzw.generatesql.entity.SQLConfig;
import com.zzw.generatesql.exception.KeywordError;
import com.zzw.generatesql.exception.NullColumnException;
import com.zzw.generatesql.utils.KeywordUtil;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CreateTableService {

    /**
     * 下载excel模板文件
     * @param outputStream
     */
    public void downloadTemplate(OutputStream outputStream) {
        EasyExcel.write(outputStream,ColumnName.class)
                .sheet()
                .doWrite(() -> new ArrayList<ColumnName>());
    }

    public String generateCreateTable(String fileName) throws KeywordError, NullColumnException {

        return generateCreateTable(fileName,null);
    }

    public String generateCreateTable(String fileName,SQLConfig sqlConfig) throws KeywordError, NullColumnException {

        if (sqlConfig == null) {
            sqlConfig = new SQLConfig();
            sqlConfig.setDefaultValue();
        }

        List<ColumnName> data = readExcel(fileName);
        return generateCreateTable(data,sqlConfig);
    }

    public String generateCreateTable(InputStream inputStream) throws KeywordError, NullColumnException {

        return generateCreateTable(inputStream,null);
    }

    public String generateCreateTable(InputStream inputStream,SQLConfig sqlConfig) throws KeywordError, NullColumnException {

        if (sqlConfig == null) {
            sqlConfig = new SQLConfig();
            sqlConfig.setDefaultValue();
        }

        List<ColumnName> data = readExcel(inputStream);
        return generateCreateTable(data,sqlConfig);
    }

    /**
     * 通过文件名读取excel中的内容
     * @param fileName
     * @return
     */
    private List<ColumnName> readExcel(String fileName) {
        List<ColumnName> data = new LinkedList<>();
        EasyExcel.read(fileName,ColumnName.class,new PageReadListener<ColumnName>(dataList -> {
            for (ColumnName columnName : dataList) {
                data.add(columnName);
            }
        })).sheet().doRead();

        return data;
    }

    /**
     * 通过文件输入流读取excel中的内容
     * @param inputStream
     * @return
     */
    private List<ColumnName> readExcel(InputStream inputStream) {
        List<ColumnName> data = new LinkedList<>();
        EasyExcel.read(inputStream,ColumnName.class,new PageReadListener<ColumnName>(dataList -> {
            for (ColumnName columnName : dataList) {
                data.add(columnName);
            }
        })).sheet().doRead();

        return data;
    }

    public String generateCreateTable(List<ColumnName> data) throws KeywordError, NullColumnException {

        SQLConfig sqlConfig = new SQLConfig();
        sqlConfig.setDefaultValue();
        return generateCreateTable(data,sqlConfig);
    }

    public String generateCreateTable(List<ColumnName> data, SQLConfig sqlConfig) throws NullColumnException, KeywordError {
        if (data == null) throw new NullColumnException("列数据不能为空");
        if (sqlConfig == null) {
            sqlConfig = new SQLConfig();
        }
        sqlConfig.setDefaultValue();
        KeywordUtil keywordUtil = new KeywordUtil(sqlConfig.getKeywordMode());

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(keywordUtil.getKeyword("drop"));
        stringBuilder.append(" ");
        stringBuilder.append(keywordUtil.getKeyword("table"));
        stringBuilder.append(" ");
        stringBuilder.append(keywordUtil.getKeyword("if"));
        stringBuilder.append(" ");
        stringBuilder.append(keywordUtil.getKeyword("exists"));
        stringBuilder.append(" ");
        stringBuilder.append(sqlConfig.getTableName());
        if (sqlConfig.getHaveSemicolon() == HaveSemicolon.HAVE) {
            stringBuilder.append(";");
        }
        stringBuilder.append("\n");
        stringBuilder.append(keywordUtil.getKeyword("create"));
        stringBuilder.append(" ");
        stringBuilder.append(keywordUtil.getKeyword("table"));
        stringBuilder.append(" ");
        stringBuilder.append(sqlConfig.getTableName());
        stringBuilder.append("(\n");
        for (int i=0 ; i<data.size() ; i++ ) {
            ColumnName columnName = data.get(i);
            stringBuilder.append("    ");
            stringBuilder.append(columnName.getFieldName());
            stringBuilder.append(" ");
            stringBuilder.append(columnName.getDataType());

            //处理字段是否非空
            if (columnName.getNotNull() != null && !columnName.getNotNull().trim().equals("")) {
                if ("是".equals(columnName.getNotNull().trim()) || "null".equalsIgnoreCase(columnName.getNotNull().trim())) {
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("null"));
                } else if ("否".equals(columnName.getNotNull().trim()) || "not null".equalsIgnoreCase(columnName.getNotNull().trim())) {
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("not"));
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("null"));
                }
            }

            //设置唯一性约束
            if ( columnName.getUnique()!=null && "是".equals(columnName.getUnique().trim()) ) {
                stringBuilder.append(" ");
                stringBuilder.append(keywordUtil.getKeyword("unique"));
            }

            //check约束
            if (columnName.getCheck()!=null && !"".equals(columnName.getCheck().trim())) {
                String check = columnName.getCheck().trim();
                stringBuilder.append(" ");
                stringBuilder.append(keywordUtil.getKeyword("check"));
                stringBuilder.append(" ");
                char c1 = check.charAt(0);
                char c2 = check.charAt(check.length()-1);
                if (c1 != '(') {
                    stringBuilder.append("(");
                }
                stringBuilder.append(check);
                if (c2 != ')') {
                    stringBuilder.append(")");
                }
            }

            //设置默认值
            if (columnName.getDefaultData()!=null && !"".equals(columnName.getDefaultData().trim())) {
                String defaultData = columnName.getDefaultData().trim();
                stringBuilder.append(" ");
                stringBuilder.append(keywordUtil.getKeyword("default"));
                stringBuilder.append(" ");
                stringBuilder.append(defaultData);
            }

            //行内设置主键
            if (sqlConfig.getPrimaryMode() == ObligatoryMode.MIXED && columnName.getPrimaryKey()!=null) {
                if ( "是".equals(columnName.getPrimaryKey().trim()) ) {
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("primary"));
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("key"));
                }
            }

            //设置行内注释
            if (sqlConfig.getCommentMode() == ObligatoryMode.MIXED) {
                String s1 = columnName.getFieldDescription();
                s1 = (s1==null) ? "" : s1.trim();
                String s2 = columnName.getRemark();
                s2 = (s2==null) ? "" : s2.trim();

                String ss = "";
                if (!s1.equals("")) {
                    ss += s1;
                }
                if (!s2.equals("") && sqlConfig.getHaveRemark() == HaveRemark.YES) {
                    if (!ss.equals("")) {
                        ss += ",";
                    }
                    ss += s2;
                }

                if (!ss.equals("")) {
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("comment"));
                    stringBuilder.append(" ");
                    stringBuilder.append("'");
                    stringBuilder.append(ss);
                    stringBuilder.append("'");
                }
            }

            //处理行末逗号
            if (i < data.size()-1) {
                stringBuilder.append(",");
            }
            stringBuilder.append("\n");
        }


        //处理一些其他内容，比如单独的主键设置
        //行外设置主键
        if (sqlConfig.getPrimaryMode() == ObligatoryMode.ALONE) {
            List<String> keys = new ArrayList<>();
            //找到所有的主键
            for (ColumnName columnName : data) {
                String primaryKey = columnName.getPrimaryKey();
                if (primaryKey != null) {
                    primaryKey = primaryKey.trim();
                    if ("是".equals(primaryKey)) {
                        keys.add(columnName.getFieldName());
                    }
                }
            }
            if (keys.size() > 0) {
                //补充上一行的分号
                stringBuilder.insert(stringBuilder.length()-1,",");
                stringBuilder.append("    ");
                stringBuilder.append(keywordUtil.getKeyword("primary"));
                stringBuilder.append(" ");
                stringBuilder.append(keywordUtil.getKeyword("key"));
                stringBuilder.append(" ");
                stringBuilder.append(keywordUtil.getKeyword("("));
                for (int i=0 ; i<keys.size() ; i++) {
                    stringBuilder.append(keys.get(i));
                    if (i < keys.size()-1) {
                        stringBuilder.append(",");
                    }
                }
                stringBuilder.append(keywordUtil.getKeyword(")\n"));
            }
        }

        stringBuilder.append(")");
        if (sqlConfig.getHaveSemicolon() == HaveSemicolon.HAVE) {
            stringBuilder.append(";");
        }

        stringBuilder.append("\n");
        //设置单独注释
        if (sqlConfig.getCommentMode() == ObligatoryMode.ALONE) {

            for (int i=0 ; i<data.size() ; i++) {
                ColumnName columnName = data.get(i);
                String s1 = columnName.getFieldDescription();
                s1 = (s1==null) ? "" : s1.trim();
                String s2 = columnName.getRemark();
                s2 = (s2==null) ? "" : s2.trim();

                String ss = "";
                if (!s1.equals("")) {
                    ss += s1;
                }
                if (!s2.equals("") && sqlConfig.getHaveRemark() == HaveRemark.YES) {
                    if (!ss.equals("")) {
                        ss += ",";
                    }
                    ss += s2;
                }

                if (!ss.equals("")) {
                    stringBuilder.append(keywordUtil.getKeyword("comment"));
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("on"));
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("table"));
                    stringBuilder.append(" ");
                    stringBuilder.append(sqlConfig.getTableName());
                    stringBuilder.append("." + columnName.getFieldName());
                    stringBuilder.append(" ");
                    stringBuilder.append(keywordUtil.getKeyword("is"));
                    stringBuilder.append(" ");
                    stringBuilder.append("'");
                    stringBuilder.append(ss);
                    stringBuilder.append("'");
                    if (sqlConfig.getHaveSemicolon() == HaveSemicolon.HAVE) {
                        stringBuilder.append(";\n");
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

}
