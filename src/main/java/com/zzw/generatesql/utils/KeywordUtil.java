package com.zzw.generatesql.utils;

import com.zzw.generatesql.constant.KeywordMode;
import com.zzw.generatesql.exception.KeywordError;

public class KeywordUtil {

    private KeywordMode keywordMode;

    public KeywordUtil() {
        this.keywordMode = KeywordMode.UPPER;
    }

    public KeywordUtil(KeywordMode keywordMode) {
        this.keywordMode = keywordMode;
    }

    public String getKeyword(String keyword) throws KeywordError {
        if (keyword == null || keyword.trim().length() == 0) {
            throw new KeywordError("关键字不能是空白字符");
        }

        if (keywordMode == KeywordMode.LOWER ) {
            keyword = keyword.toLowerCase();
        } else {
            keyword = keyword.toUpperCase();
        }

        return keyword;
    }

}
