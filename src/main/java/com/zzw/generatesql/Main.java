package com.zzw.generatesql;

import com.zzw.generatesql.entity.SQLConfig;
import com.zzw.generatesql.exception.KeywordError;
import com.zzw.generatesql.exception.NullColumnException;
import com.zzw.generatesql.service.CreateTableService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws NullColumnException, KeywordError, FileNotFoundException {
        CreateTableService createTableService = new CreateTableService();


        FileInputStream inputStream = new FileInputStream("D:\\temp.xls");
        SQLConfig sqlConfig = new SQLConfig();


        String s = createTableService.generateCreateTable(inputStream);

        System.out.println(s);
    }
}
