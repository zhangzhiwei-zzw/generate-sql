package com.zzw.generatesql.exception;

public class KeywordError extends Exception{
    public KeywordError() {
        super();
    }

    public KeywordError(String message) {
        super(message);
    }

    public KeywordError(String message, Throwable cause) {
        super(message, cause);
    }

    public KeywordError(Throwable cause) {
        super(cause);
    }


    protected KeywordError(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
