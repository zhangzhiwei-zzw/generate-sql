package com.zzw.generatesql.exception;

public class NullColumnException extends Exception{

    public NullColumnException() {
        super();
    }

    public NullColumnException(String message) {
        super(message);
    }

    public NullColumnException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullColumnException(Throwable cause) {
        super(cause);
    }

    protected NullColumnException(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
