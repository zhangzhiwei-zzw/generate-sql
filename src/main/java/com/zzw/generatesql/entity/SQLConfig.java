package com.zzw.generatesql.entity;

import com.zzw.generatesql.constant.HaveRemark;
import com.zzw.generatesql.constant.HaveSemicolon;
import com.zzw.generatesql.constant.KeywordMode;
import com.zzw.generatesql.constant.ObligatoryMode;
import lombok.Data;

@Data
public class SQLConfig {

    //主键模式
    private ObligatoryMode primaryMode;

    //注解模式
    private ObligatoryMode commentMode;

    //是否拼接备注
    private HaveRemark haveRemark;

    //字段值检查模式
    private ObligatoryMode checkMode;

    //关键字模式
    private KeywordMode keywordMode;

    //有无末尾分号
    private HaveSemicolon haveSemicolon;

    //表名
    private String tableName;

    //生成的sql文件名
    private String sqlFileName;

    public void setDefaultValue() {
        primaryMode = (primaryMode == null ? ObligatoryMode.ALONE : primaryMode);
        commentMode = (commentMode == null ? ObligatoryMode.ALONE : commentMode);
        haveRemark = (haveRemark == null ? HaveRemark.YES : haveRemark);
        checkMode = (checkMode == null ? ObligatoryMode.ALONE : checkMode);
        keywordMode = (keywordMode == null ? KeywordMode.UPPER : keywordMode);
        haveSemicolon = (haveSemicolon == null ? HaveSemicolon.HAVE : haveSemicolon);
        sqlFileName = (sqlFileName == null ||  sqlFileName.equals("") ? "createtable.sql" : sqlFileName);
        tableName = (tableName == null || tableName.equals("") ? "default_table_name" : tableName);
    }
}