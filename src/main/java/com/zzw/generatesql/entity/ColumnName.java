package com.zzw.generatesql.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 生成excel时的列名由该类指定
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ColumnName {

    //字段名
    @ExcelProperty("字段名")
    private String fieldName;

    //字段描述
    @ExcelProperty("字段描述")
    private String fieldDescription;

    //数据类型
    @ExcelProperty("数据类型")
    private String dataType;

    //备注
    @ExcelProperty("备注")
    private String remark;

    //主键
    @ExcelProperty("是否主键")
    private String primaryKey;

    //非空
    @ExcelProperty("是否非空")
    private String notNull;

    //唯一
    @ExcelProperty("是否唯一")
    private String unique;

    //约束检查
    @ExcelProperty("check")
    private String check;

    //默认值
    @ExcelProperty("默认值")
    private String defaultData;

}
